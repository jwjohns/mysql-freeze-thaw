#!/bin/bash
# all credits go to Jelmer Jaarsma, http://nl.linkedin.com/pub/jelmer-jaarsma/8/456/717

source /root/backup/settings

#Vmware made its snapshot, remove WAITFORSNAPSHOT file. Locktables background script should detect this file and exit.
rm ${WAITFORSNAPSHOT}
