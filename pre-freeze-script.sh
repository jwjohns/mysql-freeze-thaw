#!/bin/bash
# all credits go to Jelmer Jaarsma, http://nl.linkedin.com/pub/jelmer-jaarsma/8/456/717

source /root/backup/settings

if [ -e ${WAITFORLOCK} ]; then
	echo Previous backup failed, waitforlock file still present && exit 1
fi

if [ -e ${WAITFORSNAPSHOT} ]; then
	echo Previous backup failed, WAITFORSNAPSHOT file still present && exit 1
fi

if [ -e ${LOCKTABLERUN} ]; then
	ps -p `cat ${LOCKTABLERUN}` > /dev/null 2>&1;
	if [ $? -eq 0 ]; then
		echo Panic, locktables script still running && exit 1
	else
		rm -f ${LOCKTABLERUN}
	fi		
fi

#Create waitforlock file
touch ${WAITFORLOCK}

#Launch background script to flush tables and keep database locked
/root/backup/locktables.sh &

#Store locktables pid file
LOCKTABLEPID=$!
echo ${LOCKTABLEPID} > ${LOCKTABLERUN}

#Loop until background script exits and/or reports that tables are locked
while [ -e ${WAITFORLOCK} ]; do
	ps -p ${LOCKTABLEPID} > /dev/null 2>&1;
	if [ $? -eq 1 ]; then
		break
	fi
	sleep 1
done

if [ -e ${WAITFORLOCK} ]; then
	echo Tablelock script exited without removing waitforlock file, something went wrong
else
	echo Tables are locked
fi

#Background script is running and will keep tables locked, exit without error so Veeam can make snapshot
exit 0

