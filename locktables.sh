#!/bin/bash
# all credits go to Jelmer Jaarsma, http://nl.linkedin.com/pub/jelmer-jaarsma/8/456/717

source /root/backup/settings

#Locktables
#Create waitforsnapshot file
#remove waitforlock file so prebackup script can exit and VMware can do backup
#Loop until waitforsnapshot file is deleted by post-thaw
#Unlock tables

(
    echo "FLUSH TABLES WITH READ LOCK;" && \
    sleep 5 && \
    touch ${WAITFORSNAPSHOT} && \
    rm -f ${WAITFORLOCK} && \
    while [ -e ${WAITFORSNAPSHOT} ]; do sleep 1; done && \
    echo "SHOW MASTER STATUS;" && \
    echo "UNLOCK TABLES;" && \
    echo "\quit" \
) | mysql --defaults-file=/root/.my.cnf

#Cleanup pid file
rm -f ${LOCKTABLERUN}
